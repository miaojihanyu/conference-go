from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    photo = {}
    try:
        photo["picture_url"] = content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        photo["picture_url"] = None
    return photo


def get_weather(city, state):
    url_geo = "http://api.openweathermap.org/geo/1.0/direct?q=}"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}

    geo_params = {
        "q": f"{city} {state}",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    response = requests.get(url_geo, params=geo_params, headers=headers)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    url_current = "https://api.openweathermap.org/data/2.5/weather?lat="
    current_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }

    response = requests.get(
        url_current, params=current_params, headers=headers
    )

    content = json.loads(response.content)
